package com.example.grocerylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.grocerylist.model.GroceryItem;
import com.example.grocerylist.service.GroceryItemService;


import lombok.AllArgsConstructor;





@RestController
@RequestMapping(value="/api")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@CrossOrigin(origins = "*")
public class GroceryItemController {
	
private final GroceryItemService itemService;

	@GetMapping("/grocery-lists")
	public ResponseEntity<List<GroceryItem>> getAllGroceryItems(){
		List<GroceryItem> Items = itemService.findAllGroceyList();
		return new ResponseEntity<>(Items, HttpStatus.OK);
		
	}
	@PostMapping("/grocery-lists/items")
	public ResponseEntity<GroceryItem> createItemlist(@RequestBody GroceryItem itemId){
		GroceryItem newItem = itemService.addGroceryItem(itemId);
		return new ResponseEntity<>(newItem, HttpStatus.CREATED);
	}

	@DeleteMapping("/grocery-lists/items/{item_id}")
	public ResponseEntity<GroceryItem> deleteEmployee(@PathVariable("item_id") int item_id){
		 itemService.deleteGroceryItem(item_id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
