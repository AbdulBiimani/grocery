package com.example.grocerylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.grocerylist.model.GroceyList;

import com.example.grocerylist.service.GroceryListService;


import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value="/api/")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@CrossOrigin(origins = "*")
public class GroceryListController {
	
private final  GroceryListService listService;
	

	@GetMapping("/grocery-lists")
	public ResponseEntity<List<GroceyList>> getAll(){
		List<GroceyList> glist = listService.findAllGroceyList();
		return new ResponseEntity<>(glist, HttpStatus.OK);
		
	}
	@PostMapping("/grocery-lists")
	public ResponseEntity<GroceyList> createGroceryList(@RequestBody GroceyList glist){
		GroceyList newglist = listService.createGroceryList(glist);
		return new ResponseEntity<>(newglist, HttpStatus.CREATED);
	}

	@DeleteMapping("/grocery-lists")
	public ResponseEntity<GroceyList> deleteEmployee(@PathVariable("groceryListId") int groceryListId){
		 listService.deleteGroceryList(groceryListId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
