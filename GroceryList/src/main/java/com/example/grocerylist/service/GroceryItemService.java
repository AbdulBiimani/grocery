package com.example.grocerylist.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.grocerylist.model.GroceryItem;
import com.example.grocerylist.repository.GroceryItemRepository;

@Service
public class GroceryItemService {
	private final GroceryItemRepository itemrepo;
	@Autowired
	public GroceryItemService(GroceryItemRepository itemrepo) {
		this.itemrepo = itemrepo;
	}
	public List <GroceryItem> findAllGroceyList(){
		
		   return itemrepo.findAll();
			
		}

	public GroceryItem addGroceryItem(GroceryItem itemList) {
		
		return itemrepo.save(itemList);
	}

	public void deleteGroceryItem(int itemId) {
		itemrepo.deleteById(itemId);
	}

}
