package com.example.grocerylist.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.grocerylist.model.GroceyList;
import com.example.grocerylist.repository.GroceryListRepository;







@Service
public class GroceryListService {
	
	private final GroceryListRepository g_listrepo;
	@Autowired
	public GroceryListService(GroceryListRepository g_listrepo) {
		this.g_listrepo = g_listrepo;
	}
	
public List <GroceyList> findAllGroceyList(){
		
	   return g_listrepo.findAllGroceyList();
		
	}

public GroceyList createGroceryList(GroceyList glist) {
	
	return g_listrepo.createGroceryList(glist);
}

public void deleteGroceryList(int groceryListId) {
	g_listrepo.deleteById(groceryListId);
}
}
