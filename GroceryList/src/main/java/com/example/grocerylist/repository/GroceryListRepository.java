package com.example.grocerylist.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.grocerylist.model.GroceryItem;
import com.example.grocerylist.model.GroceyList;

@Repository
public interface GroceryListRepository extends JpaRepository<GroceryItem, Integer>{
	
public List <GroceyList> findAllGroceyList();

public GroceyList createGroceryList(GroceyList list);

public void deleteGroceryList(int itemId);

}
