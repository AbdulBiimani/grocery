package com.example.grocerylist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.grocerylist.model.GroceryItem;


@Repository
public interface GroceryItemRepository extends JpaRepository<GroceryItem, Integer> {

	public GroceryItem createItemlist(GroceryItem itemList);
	
	public void deleteGroceryItem(int itemId);
	

}
