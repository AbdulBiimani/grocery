package com.example.grocerylist.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="GroceryList")
public class GroceyList {
	
	public GroceyList(String groceryListName) {
		super();
		this.groceryListName = groceryListName;
	}
	
	

	public GroceyList(String groceryListName, List<GroceryItem> itemList) {
		super();
		this.groceryListName = groceryListName;
		ItemList = itemList;
	}



	@Id
	@Column(name="GroceryListId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
//	@OneToMany(mappedBy="GroceryListId", fetch=FetchType.LAZY,  cascade = CascadeType.ALL)
	private int groceryListId;
	
	@Column(name="List_name")
	private String groceryListName;
	
	@OneToMany(mappedBy="groceryListId", fetch=FetchType.LAZY,  cascade = CascadeType.ALL)
	private List<GroceryItem> ItemList = new ArrayList<>();

}
