package com.example.grocerylist.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor


@Entity
@Table(name="GroceryItem")
public class GroceryItem {
	@Id
	@Column(name="Item_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private int itemId;
	
	@Column(name="Item_name")
	private String itemName;
	
	@Column(name="Item_cost")
	private double itemCost;
	
	@Column(name ="Item_type")
	private String itemType;
	
//	@ManyToOne(fetch=FetchType.EAGER)
//	@JoinColumn(name="groceryid_fk")
//	private GroceyList groceryListId;
	
	
	public GroceryItem(String itemName, double itemCost, String itemType) {
		super();
		this.itemName = itemName;
		this.itemCost = itemCost;
	}

	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="groceryItem_fk")
	@JsonIgnore
	private GroceyList groceryListId;
	
	

}
